# EDFlib_nrf52840

EDFlib for nrf52840

## Getting started

EDFlib is a programming library for C/C++ for reading and writing EDF+ and BDF+ files.
It also reads "old style" EDF and BDF files.
EDF means European Data Format. BDF is the 24-bits version of EDF.

The library consists of only two files: edflib.h and edflib.c.

Forked from Teuniz / EDFlib (https://gitlab.com/Teuniz/EDFlib)


This file is used together with FatFs library and can generate EDF files on TF card.
This files based on nrf52840 sdk 15.3.0(the sdk include FatFs library, very nice.).

TODO: you can change the function "time(NULL)" to youself  own.
